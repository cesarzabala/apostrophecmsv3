module.exports = {
  SECRET: process.env.SECRET,
  API_KEY: process.env.API_KEY
};
