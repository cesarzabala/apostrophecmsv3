module.exports = {
  extend: '@apostrophecms/piece-type',
  options: {
    label: 'Category',
    pluralLabel: 'Categories'
  },
  fields: {
    add: {
      title: {
        label: 'Category Name',
        type: 'string'
      },
      subtitle: {
        label: 'Subtitle',
        type: 'string'
      },
      image: {
        label: 'Thumbnail',
        type: 'attachment',
        required: true
      }
    },
    group: {
      basics: {
        label: 'Category Information',
        fields: [ 'subtitle', 'image' ]
      }
    }
  }
};
