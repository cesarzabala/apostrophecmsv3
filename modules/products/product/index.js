module.exports = {
  extend: '@apostrophecms/piece-type',
  options: {
    label: 'Product',
    pluralLabel: 'Products',
    quickCreate: true
  },
  components(self) {
    return {
      async newest(req, data) {
        // Using the `find` method with query builders
        const products = await self
          .find(req)
          .sort({
            createdAt: -1
          })
          .limit(data.max || 2)
          .toArray();

        return {
          products
        };
      }
    };
  },
  fields: {
    add: {
      title: {
        label: 'Product Name',
        type: 'string'
      },
      image: {
        label: 'Thumbnail',
        type: 'attachment',
        required: true
      },
      shortDescription: {
        label: 'Short Description',
        type: 'string',
        textarea: true,
        required: true
      },
      description: {
        label: 'Description',
        type: 'area',
        options: {
          widgets: {
            '@apostrophecms/rich-text': {}
          }
        }
      },
      price: {
        label: 'Price',
        type: 'float',
        required: true
      },
      promotionalPrice: {
        label: 'Promotional Price',
        type: 'float'
      },
      available: {
        label: 'Is available',
        type: 'boolean',
        required: true
      },
      quantity: {
        label: 'Quantity',
        type: 'integer'
      },
      sku: {
        label: 'SKU',
        type: 'string'
      },
      upc: {
        label: 'UPC',
        type: 'string'
      },
      _categories: {
        label: 'Categories',
        type: 'relationship',
        withType: 'category',
        min: 1,
        max: 2,
        builders: {
          project: {
            title: 1,
            _url: 1
          }
        }
      },
      rating: {
        type: 'starRating',
        label: 'Star Rating',
        required: true
      }
    },
    group: {
      basics: {
        label: 'Products Information',
        fields: ['shortDescription', 'description', 'rating', 'image']
      },
      prices: {
        label: 'Prices',
        fields: ['price', 'promotionalPrice']
      },
      inventory: {
        label: 'Inventory',
        fields: ['available', 'quantity', 'sku', 'upc']
      },
      utility: {
        label: 'Category',
        fields: ['_categories']
      }
    }
  },
  // Columns in the modal manager
  columns: {
    add: {
      rating: {
        label: 'Rating',
        component: 'ColumnStarRating'
      }
    }
  }
};
