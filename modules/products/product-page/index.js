module.exports = {
  extend: '@apostrophecms/piece-page-type',
  options: {
    label: 'Product Page',
    pluralLabel: 'Product Pages',
    pieceModuleName: 'product'
  },
  fields: {
    add: {},
    group: {}
  },
  init(self) {
    self.apos.template.addFilter({
      currency: self.currency,
      currencyNew: self.currencyNew
    });
  },
  methods(self) {
    return {
      currency(s) {
        return new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'USD'
        }).format(s);
      },
      currencyNew(s) {
        return `${s}....`;
      }
    };
  }
};
