module.exports = {
  fields: {
    add: {
      githubUrl: {
        type: 'url',
        label: 'Github organization url'
      },
      productColumns: {
        type: 'integer',
        label: 'Product Columns'
      },
      productsPerPage: {
        type: 'integer',
        label: 'Products Per Page'
      }
    },
    group: {
      basics: {
        label: 'Basics',
        fields: [ 'githubUrl' ]
      },
      productPage: {
        label: 'Products',
        fields: [ 'productColumns', 'productsPerPage' ]
      }
    }
  }
};
