module.exports = {
  extend: '@apostrophecms/piece-type',
  init (self) {
    // method for log
    console.log(self.fieldsGroups);
  },
  options: {
    label: 'Blog Post',
    plurallabel: 'Posts'
    // Additionally add a `pluralLabel` option if needed.
    /* publicApiProjection: {
      title: 1,
      slug: 1,
      _url: 1,
      authorName: 1,
      main: 1,
      thumbnail: 1
    } */
  },
  // The field schema
  fields: {
    add: {
      authorName: {
        label: 'Author name',
        type: 'string'
      },
      body: {
        label: 'Blog post body',
        type: 'area',
        options: {
          widgets: {
            '@apostrophecms/rich-text': {}
          }
        }
      },
      attachment: {
        label: 'Attachment',
        type: 'attachment'
      },
      accept: {
        label: 'Terms and Conditions',
        type: 'boolean'
      },
      age: {
        label: 'Age range',
        type: 'select',
        choices: [
          { label: '18 - 25' },
          { label: '26 - 35' },
          { label: '36 or more' }
        ]
      }
    },
    group: {
      blogFields: {
        label: 'Blog fields',
        fields: [ 'title', 'authorName', 'body', 'attachment' ]
      },
      finals: {
        label: 'Finals',
        fields: [ 'accept', 'age' ]
      }
    }
  }
};
