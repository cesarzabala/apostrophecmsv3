module.exports = {
  extend: '@apostrophecms/widget-type',
  options: {
    label: 'Collapsible Section',
    icon: 'collapsable'
  },
  icons: {
    collapsable: 'ArrowCollapseDown'
  },
  fields: {
    add: {
      heading: {
        type: 'string',
        required: true
      },
      detail: {
        type: 'string',
        required: true,
        textarea: true
      },
      color: {
        type: 'select',
        choices: [
          {
            label: 'Red',
            value: 'red'
          },
          {
            label: 'Geen',
            value: 'green'
          }
        ]
      }
    }
  }
};
