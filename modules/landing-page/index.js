module.exports = {
  extend: '@apostrophecms/page-type',
  options: {
    label: 'Landing Page'
    // Additionally add a `pluralLabel` option if needed.
  },
  init(self) {
    self.apos.doc.addContextOperation(self.name, {
      context: 'update',
      action: 'myUniqueAction',
      label: 'My Menu Item',
      modal: 'MyModalComponent'
    });
  },
  fields: {
    add: {
      subtitle: {
        type: 'string',
        label: 'Subtitle',
        required: true
      },
      main: {
        type: 'area',
        options: {
          widgets: {
            '@apostrophecms/rich-text': {
              toolbar: ['styles', 'bold', 'italic', 'strike', 'link'],
              styles: [
                {
                  tag: 'p',
                  label: 'Paragraph (p)'
                },
                {
                  tag: 'h2',
                  label: 'Heading 2 (H2)',
                  class: 'centered'
                }
              ]
            },
            '@apostrophecms/image': {},
            '@apostrophecms/video': {},
            'two-columns': {},
            collapse: {}
          }
        }
      }
    },
    group: {
      basics: {
        label: 'Basics',
        fields: ['subtitle']
      },
      mainArea: {
        label: 'Main page content',
        fields: ['main']
      }
    }
  }
};
