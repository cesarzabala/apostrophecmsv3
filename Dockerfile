FROM keymetrics/pm2:16-buster

LABEL authors="Cesar Zabala"

ARG NODE_ENV
ARG APOS_MONGODB_URI
ARG SECRET
ARG API_KEY

ENV NODE_ENV=$NODE_ENV
ENV APOS_MONGODB_URI=$APOS_MONGODB_URI
ENV SECRET=$SECRET
ENV API_KEY=$API_KEY

# update dependencies and install curl
RUN apt-get update && apt-get install -y \
    curl \
    && rm -rf /var/lib/apt/lists/*

RUN npm install -g @apostrophecms/cli


ENV NPM_CONFIG_LOGLEVEL warn


# Create app directory
WORKDIR /app

COPY package*.json .
COPY pm2.json .

RUN npm ci --only=production
# RUN npm install

# Bundle app source
COPY . .

# Mount persistent storage
VOLUME ./data
VOLUME ./public/uploads

# Add the environment variable
# to copy files rather than use symlinks
ENV APOS_ALWAYS_COPY_ASSETS=1


EXPOSE 3000

#CMD [ "pm2-runtime", "start", "pm2.json" ]
CMD [ "npm", "run", "start" ]